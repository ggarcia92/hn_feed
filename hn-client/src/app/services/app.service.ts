import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Article } from '../models/article.interface';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { PaginationResponse } from '../models/pagination-response.interface';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  fetch (page: number, limit: number): Observable<PaginationResponse<Article>> {
    return this.http.get<PaginationResponse<Article>>(`${environment.apiUrl}/articles`, { 
      params: new HttpParams().set('page', page.toString()).set('limit', limit.toString())
    })
  }

  remove (id: string): Observable<Article> {
    return this.http.delete<Article>(`${environment.apiUrl}/articles/${id}`)
  }
}
