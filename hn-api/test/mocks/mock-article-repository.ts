import { Article } from "../../src/article/article.schema";
import { BulkWriteResponse } from "../../src/common/reponses/bulk-write-response";
import * as faker from 'faker';

export const article = new Article('1', 'ggarcia', new Date());

const articlesArr = new Array<Article>(faker.random.number({min: 50, max: 100}));
articlesArr.map(() => ({
  story_id: faker.random.number(),
  title: faker.lorem.sentence,
  story_title:  faker.lorem.sentence,
  url: faker.internet.url,
  story_url: faker.internet.url,
  author: faker.internet.userName,
  created_at: new Date()
}));

export const articles = articlesArr;

export const MockArticleRepository = {
  findById(id: any) {
    return {
      exec: () => Promise.resolve(id === 'not-found' ? null : article)
    }
  }, 
  updateOne (id: any, data: any) {
    return {
      exec: () => Promise.resolve(data)
    }
  },
  bulkWrite (data: Array<any>) {
    return Promise.resolve(new BulkWriteResponse(data.length))
  },
  find(conditions?: any) {
    return {
      sort: (options: any) => {
        return {
          skip: (skip: number) => {
            return {
              limit: (limit: number) => {
                return {
                  exec: () => Promise.resolve(articles.slice(skip, skip + limit))
                }
              }
            }
          }
        }
      },
      count: () => {
        return {
          exec: () => Promise.resolve(articles.length)
        }
      }
    }
  }
}