# HN Feed Services

## Run on Develop
```bash
$ docker-compose up
```

## Run to Deploy
```bash
$ docker-compose -f docker-compose-prod.yml up -d
```

### To Run each service without Docker please refer to the `README` on each folder