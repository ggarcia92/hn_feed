import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Article, ArticleSchema } from './article.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Article.name, schema: ArticleSchema }
    ])
  ],
  controllers: [
    ArticleController
  ],
  providers: [
    ArticleService
  ],
  exports: [
    ArticleService
  ]
})
export class ArticleModule { }
