export interface Article {
  _id: string;
  object_id: string;
  title?: string;
  story_title?: string;
  url?: string;
  story_url?: string;
  author: string;
  created_at: Date;
  deleted_at?: Date;
}
