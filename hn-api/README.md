# HN Api

Api for Hn Feed of Reign

## Swagger

To understand the API, please access to `http://localhost:8080/swagger`

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Environment
Please check the `.env.example` file to create your own `.env` with those variables. If you will start it with docker don't need to perform any changes to the `.env.example` variables.

## Docker

```bash
$ cd .. && docker-compose up --build hn_api
```
### NOTE: We recommend the use of our docker-compose to develop
