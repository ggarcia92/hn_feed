import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ArticleService } from '../article/article.service';
import { BulkWriteResponse } from '../common/reponses/bulk-write-response';
import axios from 'axios';

@Injectable()
export class ImportService {
  private logger: Logger;

  constructor(
    private articleService: ArticleService
  ) { 
    this.logger = new Logger(ImportService.name);
  }

  // @Cron(CronExpression.EVERY_HOUR)
  @Cron(CronExpression.EVERY_MINUTE)
  async importArticles() {
    try {
      let page = 0;
      while (true) {
        const response = await axios.get(`${process.env.QUERY}&hitsPerPage=100&page=${page}`, { responseType: "json" });
        const operations = [];
        response.data.hits.forEach(article => {
          const { objectID, ...other } = article
          if (other.story_title || other.title) {
            operations.push({
              updateOne: {
                upsert: true,
                filter: { object_id: objectID },
                update: { ...{ object_id: objectID }, ...other }
              }
            })
          }
        });
        await this.articleService.bulkWrite(operations)
        page++;
        if (page > response.data.nbPages) {
          break;
        }
      }
      this.logger.log('Finished succesfully importation from external service')
    }catch(error) {
      this.logger.error(`Error importing articles from external service. Description: ${error.message}`);
    }
  }
 }
