import { Controller, Get, Query, Delete, HttpCode, Param } from '@nestjs/common';
import { ArticleService } from './article.service';
import { Article } from './article.schema';
import { ApiOkResponse, ApiInternalServerErrorResponse, ApiQuery, ApiParam, ApiTags, ApiNotFoundResponse, ApiAcceptedResponse } from '@nestjs/swagger';
import { PaginationResult } from '../common/reponses/pagination-result';

@Controller('articles')
@ApiTags('article')
export class ArticleController {
  constructor(
    private articleService: ArticleService 
  ) {}

  @Get()
  @ApiOkResponse({ description: 'Retrieves articles successfully', type: PaginationResult })
  @ApiInternalServerErrorResponse({ description: 'Unexpected error' })
  @ApiQuery({ name: 'page', description: 'Page to retrieve' })
  @ApiQuery({ name: 'limit', description: 'Maximum amount to show' })
  async findAll (@Query('page', { transform: Number }) page: number, @Query('limit', { transform: Number }) limit: number): Promise<PaginationResult<Article>> {
    return this.articleService.findAll(page, limit);
  }

  @Delete('/:id')
  @HttpCode(202)
  @ApiAcceptedResponse({ description: 'Article delete successfully', type: Article })
  @ApiNotFoundResponse({ description: 'Article don\'t founded' })
  @ApiInternalServerErrorResponse({ description: 'Unexpected error' })
  @ApiParam({ name: 'id', description: 'Identifier of the article to delete', type: String })
  async delete (@Param('id') id: string): Promise<Article> {
    return this.articleService.softDelete(id);
  }
 }
