import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop({ unique: true, required: true })
  @ApiProperty({ description: 'Article ID in external service' })
  object_id: string;

  @Prop({ default: null })
  @ApiPropertyOptional({ description: 'Article title' })
  title: string;

  @Prop({ default: null })
  @ApiPropertyOptional({ description: 'Second article title' })
  story_title: string;

  @Prop({ default: null })
  @ApiPropertyOptional({ description: 'URL to full article' })
  url: string;

  @Prop({ default: null })
  @ApiPropertyOptional({ description: 'Second URL to full article' })
  story_url: string;

  @Prop({ required: true })
  @ApiProperty({ description: 'Article author' })
  author: string;

  @Prop({ required: true })
  @ApiProperty({ description: 'Article created datetime' })
  created_at: Date;

  @Prop({ default: null })
  @ApiPropertyOptional({ description: 'Article deleted datetime' })
  deleted_at: Date;

  constructor(object_id: string, author: string, created_at: Date, title?: string, story_title?: string, url?: string, story_url?: string, deleted_at?: Date) {
    this.object_id = object_id;
    this.title = title;
    this.story_title = story_title;
    this.url = url;
    this.story_url = story_url;
    this.author = author;
    this.created_at = created_at;
    this.deleted_at = deleted_at;
  }
}

export const ArticleSchema = SchemaFactory.createForClass(Article);