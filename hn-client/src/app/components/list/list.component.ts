import { Component } from '@angular/core';
import { Article } from '../../models/article.interface';
import { IPageInfo } from 'ngx-virtual-scroller';
import { AppService } from '../../services/app.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  protected page: number;
  protected limit: number;
  buffer: Article[];
  loading: boolean;
  empty: boolean;

  constructor(private appService: AppService) {
    this.buffer = [];
    this.limit = 10;
    this.page = 1;
  }

  fetchMore (event: IPageInfo) {
    if (event.endIndex !== this.buffer.length - 1 || this.empty || this.loading) return;
    this.loading = true;
    this.page = this.buffer.length / this.limit + 1
    this.fetchChunk()
  } 

  remove (id: string) {
    this.appService.remove(id).subscribe(() => {
      this.buffer.splice(this.buffer.findIndex(obj => obj._id === id), 1)
      Swal.fire('Success', 'The article was successfully removed', 'success');
    },
      () => {
        Swal.fire('Error', 'Error removing the article', 'error');
      })
  }

  async fetchChunk () {
    this.appService.fetch(this.page, this.limit).subscribe(res => {
      this.empty = res.data.length === 0;
      this.buffer = this.buffer.concat(res.data);
      this.loading = false;
    },
      () => {
        this.loading = false;
        Swal.fire('Error', 'Error retrieving the article', 'error');
      })
  }
}
