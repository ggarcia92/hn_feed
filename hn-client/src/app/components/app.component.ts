import { Component, OnInit } from '@angular/core';
import { AppService } from '../services/app.service';
import { Article } from '../models/article.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor () { }
}
