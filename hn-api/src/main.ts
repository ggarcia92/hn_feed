import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as compression from 'compression';
import * as RateLimit from 'express-rate-limit';
import * as helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(helmet());
  app.use(
    RateLimit({
      windowMs: 5 * 60 * 1000,
      max: 100,
    }),
  );
  app.use(compression());
  app.enableCors({
    origin: '*'
  });

  const configService = app.get(ConfigService);
  const port = configService.get<number>('PORT');
  const address = configService.get('ADDRESS');

  const config = new DocumentBuilder()
    .setTitle('HN API')
    .setDescription('HN API - Reign')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);

  await app.listen(port, address);
  
  console.log(`Application is running on: ${address}:${port}`);
}
bootstrap();
