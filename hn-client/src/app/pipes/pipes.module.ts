import { NgModule } from '@angular/core';
import { TimePipe } from './time.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TimePipe
  ],
  exports: [
    TimePipe
  ]
})
export class PipesModule { }
