import { ApiResponseProperty } from "@nestjs/swagger";

export class PaginationResult <T> {
  @ApiResponseProperty()
  page: number;

  @ApiResponseProperty()
  limit: number;

  @ApiResponseProperty()
  total: number;

  @ApiResponseProperty()
  data: Array<T>;
}
