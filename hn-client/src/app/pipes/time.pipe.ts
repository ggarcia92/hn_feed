import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const date = new Date(value);
    const now = new Date();
    if (now.getDate() - date.getDate() === 0) {
      return `${date.getHours() > 12 ? Math.abs(12 - date.getHours()) : date.getHours()}:${date.getMinutes().toString().length === 1 ? date.getMinutes().toString() + '0' : date.getMinutes()} ${date.getHours() >= 12 ? 'pm' : 'am'}`
    } else if (now.getDate() - date.getDate() === 1) {
      return 'Yesterday'
    } else {
      const splittedDate = date.toDateString().split(' ');
      return `${splittedDate[1]} ${splittedDate[2]}`
    }
  }

}
