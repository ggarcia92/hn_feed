import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
const fs = require('fs-extra');
import { join } from 'path';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "info"', async () => {
      const { version, name, description } = await fs.readJson(join(__dirname, '..', 'package.json'));
      const result = await appController.info();
      expect(result).toStrictEqual({
        name,
        version,
        description
      });
    });
  });
});