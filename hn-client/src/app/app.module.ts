import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './components/app.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { ListComponent } from './components/list/list.component';
import { AppService } from './services/app.service';
import { PipesModule } from './pipes/pipes.module';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';

@NgModule({
   declarations: [
      AppComponent,
      ListItemComponent,
      ListComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      PipesModule,
      VirtualScrollerModule
   ],
   providers: [
     AppService
   ],
   bootstrap: [
      AppComponent
   ]   
})
export class AppModule { }
