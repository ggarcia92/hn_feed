# HnClient

Angular Client for HN Feed of Reign

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Docker

```bash
$ cd .. && docker-compose up --build hn_client
```
### NOTE: We recommend the use of our docker-compose to develop

## Environment
If you perform any change to the configuration to start the `API` please check the `APIUrl` environment variable at `./src/environments`.