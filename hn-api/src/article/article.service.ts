import { Injectable, NotFoundException } from '@nestjs/common';
import { ArticleDocument, Article } from './article.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BulkWriteResponse } from '../common/reponses/bulk-write-response';
import { PaginationResult } from '../common/reponses/pagination-result';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel(Article.name)
    private articleModel: Model<ArticleDocument>
    ) { }

  async bulkWrite (createArticleDto: Article[]): Promise<BulkWriteResponse> {
    return this.articleModel.bulkWrite(createArticleDto);
  }

  async softDelete (id: string): Promise<Article> {
    const article = await this.articleModel.findById(id).exec();
    if (!article) {
      throw new NotFoundException('Article don\'t founded');
    }
    await this.articleModel.updateOne({ _id: id }, { deleted_at: new Date() }).exec()
    return await this.articleModel.findById(id).exec();
  }

  async findAll (page: number = 1, limit: number = 10): Promise<PaginationResult<Article>> {
    const _page = page === 0 ? 1 : page;
    const data = await this.articleModel.find({ deleted_at: null }).sort({ created_at: -1 }).skip((_page - 1) * limit).limit(limit).exec();
    const total = await this.articleModel.find({ deleted_at: null }).count().exec(); 
    return {
      page: _page,
      limit,
      data,
      total
    }
  }
 }
