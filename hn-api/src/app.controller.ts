import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiInternalServerErrorResponse } from '@nestjs/swagger';
const fs = require('fs-extra');
import { join, sep } from 'path';

@Controller()
export class AppController {
  @Get()
  @ApiOkResponse({ description: 'Show API datails' })
  @ApiInternalServerErrorResponse({ description: 'Unexpected error' })
  async info () {
    const folderName = join(__dirname, '..').split(sep).pop()
    const { version, name, description } = await fs.readJson(folderName === 'dist' ? join(__dirname, '..', '..', 'package.json') : join(__dirname, '..', 'package.json'));
    return {
      name,
      version,
      description
    }
  }
}