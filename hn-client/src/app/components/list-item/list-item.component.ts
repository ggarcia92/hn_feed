import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Article } from 'src/app/models/article.interface';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input()
  article: Article;
  @Output()
  remove = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit() {
  }

  openUrl () {
    if (!this.article.url && !this.article.story_url) {
      Swal.fire('Warning', 'There is not an url to redirect', 'warning');
    } else {
      window.open(this.article.url || this.article.story_url, '_blank')
    }
  }

  removeAction($event) {
    $event.stopPropagation()
    this.remove.emit()
  }
}
