import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from '../pipes/pipes.module';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { ListItemComponent } from './list-item/list-item.component';
import { ListComponent } from './list/list.component';
import { AppService } from '../services/app.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    PipesModule,
    VirtualScrollerModule
  ],
  declarations: [
    ListItemComponent,
    ListComponent
  ],
  providers: [
    AppService
  ],
  exports: [
    ListComponent
  ]
})
export class ComponentModule { }
