import { BulkWriteResponse } from "../../src/common/reponses/bulk-write-response"

export const MockArticleService = {
  bulkWrite: (data: Array<any>) => {
    return new BulkWriteResponse(data.length)
  }
}