export class BulkWriteResponse {
    insertedCount?: number;
    matchedCount?: number;
    modifiedCount?: number;
    deletedCount?: number;
    upsertedCount?: number;
    insertedIds?: { [index: number]: any };
    upsertedIds?: { [index: number]: any };
    result?: any;

    constructor(insertedCount?: number, matchedCount?: number, modifiedCount?: number, deletedCount?: number, upsertedCount?: number, insertedIds?: { [index: number]: any }, upsertedIds?: { [index: number]: any }, result?: any) {
      this.insertedCount = insertedCount;
      this.matchedCount = matchedCount;
      this.modifiedCount = modifiedCount;
      this.deletedCount = deletedCount;
      this.upsertedCount = upsertedCount;
      this.insertedIds = insertedIds;
      this.upsertedIds = upsertedIds;
      this.result = result;
    }
}