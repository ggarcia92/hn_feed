import { Test, TestingModule } from '@nestjs/testing';
import { ArticleService } from './article.service';
import { Article } from './article.schema';
import { MockArticleRepository, article, articles } from '../../test/mocks/mock-article-repository';
import { getModelToken } from '@nestjs/mongoose';
import { NotFoundException } from '@nestjs/common';
import { BulkWriteResponse } from '../common/reponses/bulk-write-response';

describe('ArticleService', () => {
  let articleService: ArticleService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleService,
        {
          provide: getModelToken(Article.name),
          useValue: MockArticleRepository
        }
      ],
    }).compile();

    articleService = app.get<ArticleService>(ArticleService);
  });

  it('should be created', () => {
    expect(articleService).toBeDefined();
  });

  describe('softDelete', () => {
    it('should return deleted object', async () => {
      const article = await articleService.softDelete('1');
      article.deleted_at = new Date();
      expect(article).toEqual(article);
    });
    it('should return not found exception', async () => {
      expect(articleService.softDelete('not-found')).rejects.toBeInstanceOf(NotFoundException);
    });
  });

  describe('findAll', () => {
    it('should return pagination object with articles', async () => {
      const articlesResponse = await articleService.findAll(1, 20);
      expect(articlesResponse.data.length).toEqual(20);
      expect(articlesResponse.page).toEqual(1);
      expect(articlesResponse.limit).toEqual(20);
      expect(articlesResponse.total).toEqual(articles.length);
    });
  });

  describe('bulkWrite', () => {
    it('should return array of articles', async () => {
      const articlesResponse = await articleService.bulkWrite(articles);
      expect(articlesResponse).toStrictEqual(new BulkWriteResponse(articles.length));
    });
  });
});